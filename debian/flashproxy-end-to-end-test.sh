#!/bin/bash
# End-to-end test for flashproxy client, facilitator and proxy all on one box.
# NOTE: this will destroy existing facilitator config/data/logs !!!
# NOTE: this does NOT test more complex reg methods like email and appspot.
#
# Depends: libparse-debcontrol-perl
# Usage: $0 [/path/to/flashproxy-*.changes]
#
# If no changes file is given, this script will install directly from APT,
# which may not be what you want.
#
# This test assumes the default relays (in /etc/flashproxy/facilitator-relays),
# which are external internet services, are actually running.
#
# You should probably read through this script first. We make no attempt to
# recover if things go wrong - the script will just abort and start from the
# beginning next time - so manual cleanup might be required.

TOR_SOCKS_PORT=19050

set -e
set -x

function cleanup() {
	sudo a2dissite fp-facilitator.conf || true
	sudo service apache2 stop
	sudo apt-get -y purge flashproxy-client flashproxy-facilitator node-flashproxy
	sudo rm -f /etc/apache2/sites-available/fp-facilitator.conf /etc/apache2/fp-facilitator.pem
	sudo rm -f /usr/local/share/ca-certificates/flashproxy-e2e-test_ssl-cert-snakeoil.crt
	sudo update-ca-certificates
	sudo rm -rf /tmp/flashproxy-e2e-test
}

cleanup
if [ -f "$1" -a "${1%.changes}" != "$1" ]; then
	perl - "$1" <<-'EOF' | cut '-d ' -f5 | grep '.deb$' | { cd "$(dirname "$1")" && xargs sudo dpkg -i; }
	use Parse::DebControl;
	$parser = new Parse::DebControl;
	@data = @{$parser->parse_file("{ grep -q SIGNATURE $ARGV[0] && \
	  gpg --decrypt $ARGV[0] 2>/dev/null || cat $ARGV[0]; } |", $options)};
	$files = $data[0]{'Files'};
	$files =~ s/^\n+//;
	print "$files\n";
	EOF
fi
for i in "$@"; do
	if [ "${i%.deb}" != "$i" ]; then sudo dpkg -i "$i"; fi
done
sudo apt-mark auto flashproxy-common flashproxy-proxy
sudo apt-get -y install flashproxy-client flashproxy-facilitator node-flashproxy

mkdir /tmp/flashproxy-e2e-test
cd /tmp/flashproxy-e2e-test

# generate facilitator www cert and install it as a root CA so stuff works
openssl genrsa -passout pass:xxxx -des3 -out server.key 1024
cp server.key server.key.orig
openssl rsa -passin pass:xxxx -in server.key.orig -out server.key
cat > openssl.cnf <<EOF
[ req ]
distinguished_name = req_distinguished_name
[ req_distinguished_name ]
commonName = Common Name (hostname, IP, or your name)
commonName_default = localhost
EOF
echo "" | openssl req -config openssl.cnf -new -key server.key -out server.csr
openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt
sudo cp server.key /etc/apache2/fp-facilitator.pem
cat server.crt | sudo tee -a /etc/apache2/fp-facilitator.pem
sudo ln -sf $PWD/server.crt /usr/local/share/ca-certificates/flashproxy-e2e-test_ssl-cert-snakeoil.crt
sudo update-ca-certificates

# configure facilitator
sudo sed -i -e 's/RUN_DAEMON="no"/RUN_DAEMON="yes"/g' /etc/default/fp-{facilitator,reg-decryptd}
sudo sed -i -e 's/^#\(.*\)websocket\(.*\)/\1websocket\2/g' /etc/flashproxy/facilitator-relays

sudo service fp-facilitator stop
sudo service fp-facilitator start
sudo service fp-reg-decryptd stop
sudo service fp-reg-decryptd start

sudo cp /usr/share/doc/flashproxy-facilitator/examples/fp-facilitator.conf /etc/apache2/sites-available/
sudo sed -i -e 's/ServerName .*/ServerName localhost/g' /etc/apache2/sites-available/fp-facilitator.conf

sudo a2enmod cgi # on debian this is necessary
sudo a2enmod ssl headers
sudo a2ensite fp-facilitator.conf
sudo service apache2 restart

# configure flashproxy-client
cp /usr/share/doc/flashproxy-client/torrc .
sed -i -e 's|./flashproxy-client|/usr/bin/flashproxy-client -f https://localhost --facilitator-pubkey /etc/flashproxy/reg-daemon.pub -4 --register-methods http|g' torrc
echo "SocksPort $TOR_SOCKS_PORT" >> torrc
echo "DataDirectory /tmp/flashproxy-e2e-test" >> torrc

# start tor and node-flashproxy
tor -f torrc & pid_tor=$!
# by default, nodejs disallows self-signed certs even if they are a root CA, for some reason. this envvar bypasses this check
NODE_TLS_REJECT_UNAUTHORIZED=0 flashproxy --facilitator https://localhost/ --unsafe_logging true & pid_flashproxy=$!

echo "==== Waiting 60 seconds for Tor to connect"
sleep 60
curl --socks5 localhost:$TOR_SOCKS_PORT https://check.torproject.org | grep -i congratulations

echo "==== TEST SUCCESSFUL!!! CONGRATULATIONS"

echo "==== Now cleaning up and removing all traces of the test... "
kill $pid_tor $pid_flashproxy
cd
cleanup
